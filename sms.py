# Download the helper library from https://www.twilio.com/docs/python/install

# se poate folosi cand vrem sa trimitem un mesaj de unui client sau de atentionare, sau cand cineva aplica la un job

from twilio.rest import Client


# Find your Account SID and Auth Token at twilio.com/console
# and set the environment variables. See http://twil.io/secure
account_sid = 'AC81de197d1be6eb89f6c7e012bf4b1a73'
auth_token = 'df6a667e7286cc45ca261b8da3b78635'
client = Client(account_sid, auth_token)

message = client.messages.create(
                              body='La multi ani! Te iubesc mult Mihaela mea draga!',
                              from_='+17088157767',
                              media_url=['https://demo.twilio.com/owl.png'],
                              to='+40727460990'
                          )

print(message.sid)